FROM node:6.13.1
MAINTAINER hanmingpan "414578449@qq.com"
ENV REFRESHED_AT 2018-3-25
RUN npm install pm2 -g
RUN mkdir -p /var/log/nodeapp
ADD . /opt/nodeapp/
WORKDIR /opt/nodeapp
RUN sh build.sh
VOLUME [ "/var/log/nodeapp" ]
EXPOSE 80
ENTRYPOINT [ "pm2", "start", "--no-daemon", "src/app.js" ]
