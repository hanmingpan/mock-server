# 模拟数据服务

如果有数据文件，运行`node app`启动本地服务。

启动后，访问本地地址`localhost:9090`

## 爬取模式
* 首先，在`config.js`中设置正确爬取url
* 其次，在`interface`下添加接口配置文件
* 然后运行`node crawler`获取数据
* 启动本地服务


## 自定义模式
* 先在`data/manual`下添加模拟数据
* 然后在`interface`下添加接口配置文件
* 其次在`route.js`中添加导入
* 启动本地服务


## 其他说明
* `data/crawler`爬取获得的数据
* `data/manual`自定义数据，参考`demo.js`
* `interface` 接口配置文件