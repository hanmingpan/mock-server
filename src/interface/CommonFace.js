/**
 * 常用接口
 */

const CommonFace = [
    {
        symbol: 'log',
        url: '/log',
        type: 'post',
        special: true,
    },
    {
        symbol: 'init-sys',
        url: '/user/init/:sys/',
        type: 'get',
        manual: 'init',
    },
    {
        symbol: 'init',
        url: '/user/init/',
        type: 'get',
        manual: 'init',
    },
    {
        symbol: 'init-m',
        url: '/user/m/init/:sys',
        type: 'get',
        manual: 'init',
    },
    {
        symbol: 'ocr-init',
        url: '/aliOcr/init/:sys',
        type: 'get',
        manual: 'ocrInit',
    },
    {
        symbol: 'km-shop',
        url: '/admin/get/all/:pageNum',
        type: 'get',
        manual: 'kmShop',
    },
    {
        symbol: 'km-shop-update',
        url: '/admin/update',
        type: 'post',
        manual: 'success',
    },
    {
        symbol: 'km-shop-search',
        url: '/admin/get/name',
        type: 'post',
        manual: 'kmShopSearch',
    },
    {
        symbol: 'km-shop-search-wid',
        url: '/admin/get/warehouse/:wid',
        type: 'get',
        manual: 'kmShopSearchWid',
    },
    {
        symbol: 'km-shop-isp',
        url: '/admin/get/ispAll',
        type: 'get',
        manual: 'ispList',
    },
];

module.exports = CommonFace;