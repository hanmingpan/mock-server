/**
 * report报表项目
 */

const reportFace = [
    //获取桌台meta信息
    {
        symbol: 'tableMeta',
        url: '/table/meta',
        manual: 'tableMeta',
        type: 'get',
    },
    //获取菜品Metadata
    {
        symbol: 'tableStatus',
        url: '/table/type/:groupId',
        manual: 'tableStatus',
        type: 'get',
    },
    //获取账单
    {
        symbol: 'orderId',
        url: '/order/:orderId',
        type: 'get',
    },
];


module.exports = reportFace;