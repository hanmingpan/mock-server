/**
 * 请求外部服务器接口列表
 * @symbol 唯一标识
 * @ulr 请求地址
 * @type 请求类型
 * @json 请求参数
 * @special 特殊标识，server自定义
 * @manual 返回数据来源，读取来自对应manual文件名，未定义为crawler下对应json
 * @delay 延时返回时间(ms)
 */


const CommonFace = [
    {
        symbol: 'demo',
        url: '/demo/',
        type: 'get',
        manual: 'init',
        delay: 1000,
    },
];

module.exports = CommonFace;