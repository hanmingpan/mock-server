/**
 * report报表项目
 */

const reportFace = [
    {
        symbol: 'specialDetail',
        url: '/shopCoupon/detial/:id/:wid',
        type: 'get',
    },
    {
        symbol: 'jstiket',
        url: '/oauth/jstiket',
        type: 'post',
    },
    {
      symbol: 'poem',
      url: '/poem/:id',
      manual: 'poem',
      type: 'post',
    },
  {
    symbol: 'poemList',
    url: '/poem/getAll/:page',
    manual: 'poemList',
    type: 'post',
  },
];


module.exports = reportFace;