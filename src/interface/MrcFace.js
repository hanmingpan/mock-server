/**
 * mrc
 */

const MrcFace = [
    {
        symbol: 'tableCode',
        manual: 'tableCode',
        url: '/shop/get/warehouse/table',
        type: 'get',
    },
    {
        symbol: 'setTableCode',
        manual: 'tableError',
        url: '/shop/create/warehouse/table',
        type: 'post',
    },
    {
        symbol: 'ocrConfig',
        manual: 'ocrConfig',
        url: '/mock/ocr/get/config/:wid',
        type: 'get',
    },
    {
        symbol: 'delTableCode',
        manual: 'error',
        url: '/shop/del/warehouse/table/:wid/:tableId',
        type: 'get',
    },
    {
        symbol: 'dishCode',
        manual: 'dishCode',
        url: '/shop/get/dish/warehouse/table/:tableId',
        type: 'get',
    },
];

module.exports = MrcFace;