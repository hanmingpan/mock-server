/**
 * report报表项目
 */

const reportFace = [
    //stat
    {
        symbol: 'getSysStat_day',
        special: true,
        url: '/khlReport/getSysStat/',
        type: 'post',
        json: {
            sys: 'BXC',
            from: '2016-11-10',
            to: '2016-11-10',
            statType: 'DAY',
        },
    },
    {
        symbol: 'getSysStat_week',
        special: true,
        url: '/khlReport/getSysStat/',
        type: 'post',
        json: {
            sys: 'BXC',
            from: '2016-11-07',
            to: '2016-11-13',
            statType: 'WEEK',
        },
    },
    {
        symbol: 'getSysStat_month',
        special: true,
        url: '/khlReport/getSysStat/',
        type: 'post',
        json: {
            sys: 'BXC',
            from: '2016-11-01',
            to: '2016-11-30',
            statType: 'MONTH',
        },
    },
    // detail,系统下门店列表
    {
        symbol: 'getSysStatDetail_day',
        special: true,
        url: '/khlReport/getSysStatDetail/',
        type: 'post',
        json: {
            sys: 'BXC',
            from: '2016-11-10',
            to: '2016-11-10',
            statType: 'DAY',
            wid: 120,
        },
    },
    {
        symbol: 'getSysStatDetail_week',
        special: true,
        url: '/khlReport/getSysStatDetail/',
        type: 'post',
        json: {
            sys: 'BXC',
            from: '2016-11-07',
            to: '2016-11-13',
            statType: 'WEEK',
            wid: 120,
        },
    },
    {
        symbol: 'getSysStatDetail_month',
        special: true,
        url: '/khlReport/getSysStatDetail/',
        type: 'post',
        json: {
            sys: 'BXC',
            from: '2016-11-01',
            to: '2016-11-30',
            statType: 'MONTH',
            wid: 120,
        },
    },
    // warehouse-单店详情
    {
        symbol: 'getWarehouseStat_day',
        special: true,
        url: '/khlReport/getWarehouseStat/',
        type: 'post',
        json: {
            sys: 'BXC',
            from: '2016-11-10',
            to: '2016-11-10',
            statType: 'DAY',
            wid: 120,
        },
    },
    {
        symbol: 'getWarehouseStat_week',
        special: true,
        url: '/khlReport/getWarehouseStat/',
        type: 'post',
        json: {
            sys: 'BXC',
            from: '2016-11-07',
            to: '2016-11-13',
            statType: 'WEEK',
            wid: 120,
        },
    },
    {
        symbol: 'getWarehouseStat_month',
        special: true,
        url: '/khlReport/getWarehouseStat/',
        type: 'post',
        json: {
            sys: 'BXC',
            from: '2016-11-01',
            to: '2016-11-30',
            statType: 'MONTH',
            wid: 120,
        },
    },
    // 设置多点目标营收
    {
        symbol: 'setExpects',
        manual: 'reportSet',
        url: '/khlReport/set/expects/',
        type: 'post',
    },
    // 设置单店营收目标
    {
        symbol: 'setExpect',
        manual: 'reportSet',
        url: '/khlReport/set/expect/',
        type: 'post',
    },
    // 设置累计销售额
    {
        symbol: 'fixAccu',
        manual: 'reportSet',
        url: '/khlReport/fix/accu/',
        type: 'post',
    },
    // 设置座位
    {
        symbol: 'desk',
        manual: 'success',
        url: '/khlReport/desk/',
        type: 'post',
    },
    // 获得设置门店列表
    {
        symbol: 'getExpects',
        manual: 'expectList',
        url: '/khlReport/get/expects/',
        type: 'get',
    },
    // 会员分组
    {
        symbol: 'groupInfo',
        manual: 'groupInfo',
        url: '/khlReport/group/info/',
        type: 'get',
    },
];


module.exports = reportFace;