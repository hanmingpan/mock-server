/**
 * 阿里项目
 */

const reportFace = [
    // 会员分组
    {
        symbol: 'houseList',
        manual: 'houseList',
        url: '/shop/query/',
        type: 'get',
    },
    {
        symbol: 'activityCreate',
        manual: 'activityCreate',
        url: '/activity/create/',
        type: 'post',
    },
    {
        symbol: 'activityCreateVoucher',
        manual: 'activityCreateVoucher',
        url: '/activity/createVoucher/',
        type: 'post',
    },
    //点菜，菜品数据
    {
        symbol: 'dish',
        manual: 'dish2',
        url: '/order/dish/:wid',
        type: 'get',
    },
    {
        symbol: 'shopCart',
        manual: 'shopCart',
        url: '/dish/shopCart',
        type: 'get',
    },
    {
        symbol: 'uploadLogo',
        manual: 'uploadLogo',
        url: '/activity/uploadLogo',
        type: 'get',
    },
    {
        symbol: 'getPrinterSort',
        manual: 'getPrinterSort',
        url: '/print/getDishTypes',
        type: 'get',
    },
    {
        symbol: 'printerMachine',
        manual: 'printerMachine',
        url: '/print/getNos',
        type: 'get',
    },
    {
        symbol: 'getPrinterSortMachine',
        manual: 'getPrinterSortMachine',
        url: '/print/getDish',
        type: 'get',
    },
    {
        symbol: 'setPrinterSort',
        manual: 'success',
        url: '/print/setDish',
        type: 'post',
    },
    {
        symbol: 'getPay',
        manual: 'success',
        url: '/print/getPay',
        type: 'get',
    },
    {
        symbol: 'userBind',
        manual: 'success',
        url: '/member/register',
        type: 'post',
        delay: 3000,
    },
];


module.exports = reportFace;