const CONFIG = {
    crawler: {
        name: 'mock-server',
        host: 'http://54.223.26.169:9090',
    },
    server: {
        port: 9090,
        delay: 0,
    }
};

module.exports = CONFIG;