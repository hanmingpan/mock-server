const moment = require('moment');
const fs = require('fs');


class Log {
    save(text){
        const now = moment();
        const date =  now.format("YYYYMMDD");
        const time =  now.format("hh-mm-ss");
        const name = './log/' + date + '.text';
        const line = '[' + time + ']' + JSON.stringify(text);

        fs.writeFile(name, line);
    }
}

module.exports = new Log();
