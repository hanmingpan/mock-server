/**
 * 数据库
 */
const fs = require('fs');
const moment = require('moment');

class Database{
    insert(symbol, url, data) {
        const save = {
            symbol: symbol,
            url: url,
            data: data,
        };
        const name = './data/crawler/' + symbol + '.json';

        fs.writeFile(name, JSON.stringify(save));
    }

    find(symbol) {
        const name = './data/crawler/' + symbol + '.json';
        const data = require(name);

        return data.data;
    }

    savePic(file){
        console.log('save', file);
        fs.appendFile('./upload/' + file);
    }

    save(data) {
        const date = moment().format('YYYYMMDD');
        const name = './data/crawler/' + date + '.json';

        fs.writeFile(name, JSON.stringify(data));
    }
}

module.exports = new Database();
