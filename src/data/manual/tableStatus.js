// 测试数据

const res = {
    status: 200,
    message: "mock",
};

// 返回数据
const data = [
    {
        tableId: 122,
        tableState: 'on',
        tableTime: '23',
        tableTotal: 541,
    },
    {
        tableId: 255,
        tableState: 'on',
        tableTime: '10',
        tableTotal: 268,
    },
    {
        tableId: 322,
        tableState: 'on',
        tableTime: '2',
        tableTotal: 211,
    },
];

res.data = data;

module.exports = res;