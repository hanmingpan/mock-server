const res = {
    status: 200,
    message: "mock",
};

// 返回数据
const data = {
    tableGroup: [
        {
            groupId: 11,
            groupName: '齐聚一堂',
        },
        {
            groupId: 22,
            groupName: "顶级包间",
        },
        {
            groupId: 33,
            groupName: "优美胜地",
        }
    ],
    table: [
        {
            tableId: 122,
            tableName: "爱因斯坦",
            tableSize: 4,
            tableCode: 'abcc',
            groupId: 11
        },
        {
            tableId: 133,
            tableName: "二号桌",
            tableSize: 12,
            tableCode: 'abdd',
            groupId: 11
        },
        {
            tableId: 144,
            tableName: "大不列颠",
            tableSize: 4,
            tableCode: 'abee',
            groupId: 11
        },
        {
            tableId: 255,
            tableName: "古罗马",
            tableSize: 4,
            tableCode: 'abff',
            groupId: 22
        },
        {
            tableId: 266,
            tableName: "雅典",
            tableSize: 4,
            tableCode: 'cdee',
            groupId: 22
        },
        {
            tableId: 277,
            tableName: "巴比伦",
            tableSize: 4,
            tableCode: 'cdff',
            groupId: 22
        },
        {
            tableId: 311,
            tableName: "库斯科",
            tableSize: 4,
            tableCode: 'fghh',
            groupId: 33
        },
        {
            tableId: 322,
            tableName: "底比斯",
            tableSize: 4,
            tableCode: 'fgii',
            groupId: 33
        },
        {
            tableId: 333,
            tableName: "西安 ",
            tableSize: 4,
            tableCode: 'fgjj',
            groupId: 33
        },
    ]
};

res.data = data;

module.exports = res;
