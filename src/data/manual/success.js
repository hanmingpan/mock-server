// 测试数据

const res = {
    status: 200,
    message: "mock-success",
};

// 返回数据
const data = {
    name: 'mock',
};

res.data = data;

module.exports = res;