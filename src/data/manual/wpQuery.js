// 测试数据

const res = {
    status: 200,
    message: "mock",
};

// 返回数据
const data = [
    {
        couponId: '123',
        couponName: '皮蛋瘦肉粥',
        shopName: '天意小馆',
        shopAddress: '王府井大街',
        totalTaken: '655',
        originPrice: '23',
        nowPrice: '8',
        rewardAmount: '1',
        imgUrl: 'http://s1.ig.meishij.net/p/20180105/5462fd3ffadde4b8c7c852cacaa781bb.jpg',
    },
    {
        couponId: '124',
        couponName: '皮蛋瘦肉粥',
        shopName: '天意小馆',
        shopAddress: '王府井大街',
        totalTaken: '655',
        originPrice: '23',
        nowPrice: '8',
        rewardAmount: '1',
        imgUrl: 'http://s1.ig.meishij.net/p/20180105/9fd0adbf02552ef1f984b460fa06bec5.jpg',
    },
    {
        couponId: '125',
        couponName: '皮蛋瘦肉粥',
        shopName: '天意小馆',
        shopAddress: '王府井大街',
        totalTaken: '655',
        originPrice: '23',
        nowPrice: '8',
        rewardAmount: '1',
        imgUrl: 'http://s1.ig.meishij.net/p/20180105/5462fd3ffadde4b8c7c852cacaa781bb.jpg',
    },
    {
        couponId: '126',
        couponName: '皮蛋瘦肉粥',
        shopName: '天意小馆',
        shopAddress: '王府井大街',
        totalTaken: '655',
        originPrice: '23',
        nowPrice: '8',
        rewardAmount: '1',
        imgUrl: 'http://s1.ig.meishij.net/p/20180105/9fd0adbf02552ef1f984b460fa06bec5.jpg',
    },
    {
        couponId: '127',
        couponName: '皮蛋瘦肉粥',
        shopName: '天意小馆',
        shopAddress: '王府井大街',
        totalTaken: '655',
        originPrice: '23',
        nowPrice: '8',
        rewardAmount: '1',
        imgUrl: 'http://s1.ig.meishij.net/p/20180105/5462fd3ffadde4b8c7c852cacaa781bb.jpg',
    },
    {
        couponId: '127',
        couponName: '皮蛋瘦肉粥',
        shopName: '天意小馆',
        shopAddress: '王府井大街',
        totalTaken: '655',
        originPrice: '23',
        nowPrice: '8',
        rewardAmount: '1',
        imgUrl: 'http://s1.ig.meishij.net/p/20180105/9fd0adbf02552ef1f984b460fa06bec5.jpg',
    },
];

res.data = data;

module.exports = res;