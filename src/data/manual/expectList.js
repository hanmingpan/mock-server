// 测试数据

const res = {
    status: 200,
    message: "mock",
};

// 返回数据
const data = [
    {
        name: '唇辣号',
        wid: '11',
        // price: 12000,
    },
    {
        name: '唇辣',
        wid: '12',
    },
    {
        name: '辣号',
        wid: '13',
        // price: 12000,
    },
    {
        name: '号',
        wid: '14',
    }
];

res.data = data;

module.exports = res;