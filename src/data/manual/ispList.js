// 测试数据

const res = {
    status: 200,
    message: "mock",
};

// 返回数据
const data = [
    {
        ispName: '大圣科技有限公司',
        id: 45,
    },
    {
        ispName: '古盛发科技有限公司',
        id: 46,
    },
    {
        ispName: '赖梦德科技有限公司',
        id: 47,
    },
];

res.data = data;

module.exports = res;