// 测试数据

const res = {
    status: 200,
    message: "mock",
};

// 返回数据
const data = {
    name: 'mock',
    age: 11,
};

res.data = data;

module.exports = res;