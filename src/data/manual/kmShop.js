// km商户管理

const res = {
    status: 200,
    message: "mock",
    page: {
        currentPage: 1,
        hasNext: true,
        totalElements: 7046,
        totalPages: 705
    }
};

// 返回数据
const data = [
    {
        IsSweep: true,
        addr: "北京市朝阳区东三环北路38号院民生保险大厦3层(近呼家楼地铁)01056705860|4000101717",
        bindTime: 0,
        cashiertTroughTime: 1493233390000,
        configPrintTime: 1493233390000,
        deviceCount: 138,
        ispName: "大道餐饮服务",
        kmCode: "Rdf2k5g",
        kmStatus: false,
        operation: "管理桌台",
        orderDishType: "A",
        shopName: "北京全聚德",
        startTime: 1493233390000,
        wid: 63
    },
    {
        IsSweep: false,
        addr: "北京朝阳区北苑路甲13号北辰购物中心B1超市出口4008166188",
        bindTime: 1493233390000,
        cashiertTroughTime: 1493233390000,
        configPrintTime: 1493233390000,
        deviceCount: 12,
        ispName: "大道餐饮服务",
        kmCode: "Rdf2k5g",
        kmStatus: true,
        operation: "管理桌台",
        orderDishType: "B",
        shopName: "麻辣诱惑(北辰店)",
        startTime: 1493233390000,
        wid: 23
    },
    {
        IsSweep: true,
        addr: "北京市宣武区牛街5-2号(010)83545602",
        bindTime: 1493233390000,
        cashiertTroughTime: 1493233390000,
        configPrintTime: 1493233390000,
        deviceCount: 1,
        ispName: "大道餐饮服务",
        kmCode: "Rdf2k5g",
        kmStatus: true,
        operation: "管理桌台",
        orderDishType: "C",
        shopName: "聚宝源(牛街西里店)",
        startTime: 1493233390000,
        wid: 45
    },
];

res.data = data;

module.exports = res;