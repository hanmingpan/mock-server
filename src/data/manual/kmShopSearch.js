// km商户管理

const res = {
    status: 200,
    message: "mock",
    page: {
        currentPage: 1,
        hasNext: true,
        totalElements: 7046,
        totalPages: 705
    }
};

// 返回数据
const data = [
    {
        IsSweep: true,
        addr: "北京市宣武区牛街5-2号(010)83545602",
        bindTime: 1493233390000,
        cashiertTroughTime: 1493233390000,
        configPrintTime: 1493233390000,
        deviceCount: 1,
        ispName: "大道餐饮服务",
        kmCode: "Rdf2k5g",
        kmStatus: true,
        operation: "管理桌台",
        orderDishType: "C",
        shopName: "聚宝源(牛街西里店)",
        startTime: 1493233390000,
        wid: 45
    },
];

res.data = data;

module.exports = res;