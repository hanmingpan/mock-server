
// 测试数据

const res = {
    status: 200,
    message: "mock",
};

// 返回数据
const data = {
    order: {
        typeMark: 'ORDER_PRINT',
        orderMark: '结账单',
        payMethodBeginMark: '合计',
        payMethodEndMark: 'empty',
        actualMark: 'empty',
        foodBeginMark: '品项',
        foodEndMark: '退菜',
        deskNoMark: '桌号',
    },
    pre: {
        typeMark: 'PRE_PRINT',
        orderMark: '预打单',
        payMethodBeginMark: 'empty',
        payMethodEndMark: '应付金额',
        actualMark: '应付金额',
        foodBeginMark: '品名',
        foodEndMark: '退菜',
        deskNoMark: '桌号',
    }
};

res.data = data;

module.exports = res;