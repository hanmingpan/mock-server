// km商户管理

const res = {
    status: 200,
    message: "mock",
    page: {
        currentPage: 1,
        hasNext: true,
        totalElements: 7046,
        totalPages: 705
    }
};

// 返回数据
const data =
    {
        IsSweep: true,
        addr: "北京市朝阳区东三环北路38号院民生保险大厦3层(近呼家楼地铁)01056705860|4000101717",
        bindTime: 0,
        cashiertTroughTime: 1493233390000,
        configPrintTime: 1493233390000,
        deviceCount: 138,
        ispName: "大道餐饮服务",
        kmCode: "Rdf2k5g",
        kmStatus: false,
        operation: "管理桌台",
        orderDishType: "A",
        shopName: "北京全聚德",
        startTime: 1493233390000,
        wid: 63
    };

res.data = data;

module.exports = res;