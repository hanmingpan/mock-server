// 门店日报

const res = {
    status: 200,
    message: "hello",
};

const data = {
    // 合计总额
    totalSum: 106789.0,
        // 优惠总额
        couponSum: 67830,
        // 实收总额
        actualSum: 2345.00,
        // 实收现金
        actualCashSum: 3784,
        // 实收银行卡
        actualBankSum: 4566,
        // 实收微信
        actualWeChatSum: 2349,
        // 实收支付宝
        actualAliPaySum: 6739,
        // 实收点评闪惠
        actualDianSum: 6390,
        payStat: [
            {
                payMethod: '现金',
                paySum: 345,
            },
            {
                payMethod: '银行卡',
                paySum: 567,
            },
            {
                payMethod: '微信',
                paySum: 123,
            },
            {
                payMethod: '支付宝',
                paySum: 423,
            },
            {
                payMethod: '闪惠',
                paySum: 123,
            },
            {
                payMethod: '现金',
                paySum: 345,
            },
            {
                payMethod: '银行卡',
                paySum: 567,
            },
            {
                payMethod: '现金',
                paySum: 345,
            },
            {
                payMethod: '银行卡',
                paySum: 567,
            },
            {
                payMethod: '微信',
                paySum: 123,
            },
            {
                payMethod: '支付宝',
                paySum: 423,
            },
            {
                payMethod: '闪惠',
                paySum: 123,
            },
            {
                payMethod: '现金',
                paySum: 345,
            },
            {
                payMethod: '银行卡',
                paySum: 567,
            },
        ],
        // 统计时间
        statTime: 1472715759037,
        // 订单总数
        orderSum: 1234,
        // 顾客总数
        consumerSum: 51234,
        // 较上周比较率
        weekRate: 3.24,
        // 总桌数
        deskSum: 234,
        // 总座位数
        deskSeatSum: 123,
        // 午市营业额
        noonSum: 23890,
        // 晚市营业额
        eveningSum: 23896,
        // 桌人数
        consumeToOrder: 123,
        // 开台情况统计
        openDesk: [
            {
                hour: 10,
                openDesk: 10,
                payDesk: 12
            },
            {
                hour: 11,
                openDesk: 12,
                payDesk: 45
            },
            {
                hour: 12,
                openDesk: 12,
                payDesk: 17
            },
            {
                hour: 13,
                openDesk: 3,
                payDesk: 30
            },
            {
                hour: 14,
                openDesk: 18,
                payDesk: 20
            },
            {
                hour: 15,
                openDesk: 12,
                payDesk: 28
            },
        ],
        // 合计总额
        actualMonthSum: 12786,
        // 订单总数
        orderMonthSum: 2389,
        // 顾客总数
        consumerMonthSum: 19856,
        // 桌单价
        deskPrice: 12907,
        // 翻台率
        deskRate: 23.34,
        // 客单价
        consumePrice: 1238,
        // 翻席率
        consumeRate: 39.45,
        // 午市单数
        noonOrderSum: 480,
        // 晚市单数
        eveningOrderSum: 6789,
};

res.data = data;

module.exports = res;