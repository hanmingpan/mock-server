// 测试数据

const res = {
    status: 200,
    message: "mock",
};

// 返回数据
const data = [
    {
        shopId: '122',
        name: '大通西路店'
    },
    {
        shopId: '122',
        name: '大通西路店'
    },
    {
        shopId: 133,
        name: '中关村苏州街店'
    },
    {
        shopId: 144,
        name: '右安门店'
    },
    {
        shopId: 155,
        name: '建国门店'
    },
];

res.data = data;

module.exports = res;