// 测试数据

const res = {
    status: 200,
    message: "mock",
};

// 返回数据
const data = [
    {
        id: 999,
        no: '2811',
        deviceId: 'ABCD',
        dishTypeId: '1'
    },
    {
        id: 999,
        no: '2822',
        deviceId: 'EFGH',
        dishTypeId: '2',
    },
];

res.data = data;

module.exports = res;