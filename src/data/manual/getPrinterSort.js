// 测试数据

const res = {
    status: 200,
    message: "mock",
};

// 返回数据
const data = [
    {
        name: '创新派',
        id: '1',
    },
    {
        name: '热菜',
        id: '2',
    },
    {
        name: '凉菜',
        id: '3',
    },
];

res.data = data;

module.exports = res;