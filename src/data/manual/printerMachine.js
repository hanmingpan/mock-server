// 测试数据

const res = {
    status: 200,
    message: "mock",
};

// 返回数据
const data = [
    {
        deviceId: 'ABCD',
        no: '2811',
    },
    {
        deviceId: 'EFGH',
        no: '2822',
    },
    {
        deviceId: 'JKLM',
        no: '2833',
    },
];

res.data = data;

module.exports = res;