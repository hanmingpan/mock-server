/**
 * 建立服务
 */

const CONFIG = require('../config/config');
const db = require('../db.js');

class Server {
    // 模块服务
    createFace(app, faceArr) {
        faceArr.map(function (elem, index) {
            if (!elem.special) {
                app[elem.type](elem.url, function (req, res) {
                    console.log(elem.symbol);

                    var manualData;
                    var delayTime = elem.delay || CONFIG.server.delay;

                    setTimeout(function () {
                        if (elem.manual) {
                            manualData = require('../data/manual/' + elem.manual);
                        } else {
                            manualData = db.find(elem.symbol);
                        }

                        res.send(manualData);
                    }, delayTime)
                });
            }
        });
    }
}

module.exports = Server;