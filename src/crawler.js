const CONFIG = require('./config/config');
const request = require('request');
const db = require('./db');
const PosFace = require('./interface/PosFace');

PosFace.map(function (elem, index) {
    if (!elem.manual) {
        const type = elem.type.toLowerCase() || 'get';
        let raw = {
            url: CONFIG.crawler.host + elem.url,
            headers: {
                'User-Agent': CONFIG.crawler.name,
            },
        };

        if (type === 'post') {
            raw.json = elem.json;
        }

        request.post(raw, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(`[${elem. symbol}] --> success --> from outside `);

                db.insert(elem.symbol, elem.url, body);
            } else {
                console.log(`[${elem.symbol}] --> failed --> from outside `, error, response.statusCode);
            }
        });
    }
});