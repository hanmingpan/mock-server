const net = require('net');
const server = net.createServer();

server.on('connection', (socket) => {
    socket.pipe(process.stdout);
    socket.write('data from server');
});
server.listen(9091, () => {
    console.log(`server is on ${JSON.stringify(server.address())}`);
});