// 路由
const db = require('./db');
const Server = require('./server/Server');
const Log = require('./log');

const CommonFace = require('./interface/CommonFace');
const ReportFace = require('./interface/ReportFace');
const MrcFace = require('./interface/MrcFace');
const AliFace = require('./interface/AliFace');
const PosFace = require('./interface/PosFace');
const WpFace = require('./interface/WpFace');

var server = new Server();

module.exports = function(app){
    var faceArr = [CommonFace, ReportFace, MrcFace, AliFace, PosFace, WpFace];

    faceArr.map(function (elem) {
        server.createFace(app, elem);
    });

    // app.post('/log', function(req, res){
    //     console.log(req.body);
    //     Log.save(req.body);
    //
    //     const successRes = require('./data/manual/success');
    //     res.send(successRes);
    // });

    // 系统门店统计
    app.post('/khlReport/getSysStat/', function(req, res){
        console.log(req.body);

        if (req.body.statType === 'DAY') {
            let data = db.find('getSysStat_day');

            res.send(data);
        }
        if (req.body.statType === 'WEEK') {
            let data = db.find('getSysStat_week');

            res.send(data);
        }
        if (req.body.statType === 'MONTH') {
            let data = db.find('getSysStat_month');

            res.send(data);
        }
    });
    // 门店列表
    app.post('/khlReport/getSysStatDetail/', function(req, res){
        console.log(req.body);

        if (req.body.statType === 'DAY') {
            let data = db.find('getSysStatDetail_day');

            res.send(data);
        }
        if (req.body.statType === 'WEEK') {
            let data = db.find('getSysStatDetail_week');

            res.send(data);
        }
        if (req.body.statType === 'MONTH') {
            let data = db.find('getSysStatDetail_month');

            res.send(data);
        }
    });

    // 单店详情
    app.post('/khlReport/getWarehouseStat/', function(req, res){
        console.log(req.body);

        if (req.body.statType === 'DAY') {
            let data = db.find('getWarehouseStat_day');

            res.send(data);
        }
        if (req.body.statType === 'WEEK') {
            let data = db.find('getWarehouseStat_week');

            res.send(data);
        }
        if (req.body.statType === 'MONTH') {
            let data = db.find('getWarehouseStat_month');

            res.send(data);
        }
    });
};
