const request = require('request').defaults({jar: true});
const uuid = require('node-uuid');

const DOMAIN = 'http://192.168.17.146:9090';


module.exports = function(app){
    const url = '/saas/base/getDeviceParams.ajax';
    const data = {
        deviceKey: ''
    };
    const header = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',

    };

    // var options = {
    //     url: DOMAIN + url,
    //     method: 'POST',
    //     form: data,
    //     headers: header,
    // };
    //
    // request(options, function (error, response, body) {
    //     console.log('[error]:', error);
    //     console.log('[statusCode]:', response && response.statusCode);
    //     console.log('[body]:', body);
    // });
    //
    // var options2 = {
    //     url: DOMAIN + '/saas/base/getShopInfo.ajax',
    //     method: 'POST',
    //     form: {},
    //     headers: header,
    // };
    // setTimeout(function () {
    //     request(options2, function (error, response, body) {
    //         console.log('[error]:', error);
    //         console.log('[statusCode]:', response && response.statusCode);
    //         console.log('[body]:', body);
    //     });
    // }, 100);

    function table() {
        var options = {
            url: DOMAIN + '/saas/order/getTableStatusLst.ajax',
            method: 'POST',
            form: {
                tableName: ''
            },
            headers: header,
        };

        request(options, function (error, response, body) {
            console.log('[error]:', error);
            console.log('[statusCode]:', response && response.statusCode);
            console.log('[body]:', body);
        });
    }

    function order() {
        var batch_id = uuid.v4();
        var item_id = uuid.v4();

        var food = {
            itemKey: '',
            itemType: "0",
            isSetFood: "0",
            isBatching: "0",
            batchingFoodTagID: "0",
            foodTagIDs: "",
            isSFDetail: "0",
            isTempFood: "0",
            isDiscount: "1",
            isNeedConfirmFoodNumber: "0",
            foodKey: "6222cfbf-ce42-4eb0-951e-49a9d6ecd54e",
            foodName: "水煮肉片",
            foodNumber: "1",
            foodSendNumber: "0",
            sendReason: "",
            unit: "份",
            unitKey: "4785985",
            foodProPrice: "100.00",
            foodPayPrice: "100.00",
            foodVipPrice: "100.00",
            foodOldPrice: "100.00",
            foodRemark: "",
            modifyReason: "",
            parentFoodFromItemKey: "",
            foodSetDetailProPrice: "",
            tasteIsRequired: "0",
            tasteIsMultiple: "1",
            makingMethodIsRequired: "0",
            makingMethodIsMultiple: "1",
            makeStatus: "1",
            unitAdjuvant: "",
            unitAdjuvantNumber: "0",
            makingMethodList: "",
            tasteList: ""
        }

        var foodLst = [];
        foodLst.push(food);

        var orderJson = {
            saasOrderKey: "20170818100550004",
            empCode: "001",
            empName: "001员工",
            bizModel: "0",
            allFoodRemark: "",
            hisFlag: "0",
            tableName: "Dinner",
            channelKey: "100_diannei",
            channelName: "店内",
            orderSubType: "0",
            person: "55",
            userName: "",
            userSex: "2",
            userMobile: "",
            userAddress: "",
            invoiceTaxRate: "0.0",
            saasOrderRemark: "",
            foodLst: foodLst
        };

        var data = {
            actionType: "LD",
            submitBatchNo: '',
            hisFlag: 0,
            orderJson: JSON.stringify(orderJson),
        };

        console.log('order', data);

        var options = {
            url: DOMAIN + '/saas/order/submitOrder.ajax',
            method: 'POST',
            form: data,
            headers: header,
        };

        request(options, function (error, response, body) {
            console.log('[error]:', error);
            console.log('[statusCode]:', response && response.statusCode);
            // console.log('[body]:', body);

            res = JSON.parse(body).data.foodLst.length;
            console.log('[res]', res);

            // table()
        });
    }

    function login() {
        var options = {
            url: DOMAIN + '/saas/emp/Login.ajax',
            method: 'POST',
            form: {
                shopName: '潮汕牛肉火锅',
                deviceCode: '',
                deviceName: '',
                deviceKey: '',
                empCode: '001',
                empPWD: '001',
            },
            headers: header,
        };

        request(options, function (error, response, body) {
            console.log('[error]:', error);
            console.log('[statusCode]:', response && response.statusCode);
            console.log('[body]:', body);

            order();
        });
    }

    // login()

    table()
};