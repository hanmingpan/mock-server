const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const CONFIG = require('./config/config');

app.set('port', process.env.PORT || CONFIG.server.port);
// 跨域
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Origin", "localhost");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By",' 3.2.1');
    res.header("Content-Type", "application/json;charset=utf-8");
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
require('./route')(app);
require('./upload')(app);
// require('./test')(app);
// require('./hua')(app);

app.listen(app.get('port'), function(){
    console.log('Mock-Server url http://localhost:' + app.get('port') + '');
});